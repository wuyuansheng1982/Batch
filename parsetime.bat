@echo off

REM For date, need to take culture into consideration

for /f "tokens=1,2,3,4 delims=/ " %%h in ('date /t') do (set mm=%%h& set dd=%%i& set yyyy=%%j& set wday=%%k)
for /f "tokens=1,2,3 delims=: " %%i in ('time /t') do (set hh=%%i& set min=%%j& set ampm=%%k)
SET hh=%hh:~0,2%
SET min=%min:~0,2%
SET CURRENTDATE=%yyyy:~0,4%%mm:~0,2%%dd:~0,2%
SET CURRENTTIME=%hh%%min%
SET AMPM=%ampm%

echo %hh%
echo %min%
echo %CURRENTDATE%
echo %cURRENTTIME%
echo %AMPM%