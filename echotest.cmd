@echo off
echo test

echo %~f1
REM script path

echo %~fs1
REM script path DOS 8.3 format

echo %~dp0
REM parent directory

echo %~n2
echo %~n0: some message
REM name of script

It means not to output the respective command. Compare the following two batch files:
@echo foo
and
echo foo
The former has only foo as output while the latter prints
H:\Stuff>echo foo
foo
(here, at least). As can be seen the command that is run is visible, too.
