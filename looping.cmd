FOR %%I IN (%USERPROFILE%\*) DO @ECHO %%I
REM GOTCHA: The FOR command uses a special variable syntax of % followed by a single letter, like %I. This syntax is slightly different when FOR is used in a batch file, as it needs an extra percent symbol, or %%I. This is a very common source of errors when writing scripts. Should your for loop exit with invalid syntax, be sure to check that you have the %% style variables.

It means not to output the respective command. Compare the following two batch files:
@echo foo
and
echo foo
The former has only foo as output while the latter prints
H:\Stuff>echo foo
foo
(here, at least). As can be seen the command that is run is visible, too.




FOR /D %I IN (%USERPROFILE%\*) DO @ECHO %I

FOR /D %I IN (%USERPROFILE%\*) DO @ECHO %I

FOR /R "%TEMP%" /D %I IN (*) DO @ECHO %I
